package com.ejab;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class ConectarPostgres {

    public Connection getConnection() throws SQLException{

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }

        Connection connection = null;

        connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/cultura", "cultura",
                    "cultura");

        return connection;

    }

}