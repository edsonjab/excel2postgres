package com.ejab;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class MapearPostgres {

    public MapearPostgres() {
        con = new ConectarPostgres();
    }

    ConectarPostgres con = null;
    Statement stmt = null;
    Connection c = null;
    int[] disertantes = {1,2,4,5,6,7,8,9,15};
    int[] evaluacion = {3,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35}; // NUMERIC 17, 25

    public int insertDisertantes(Map d) {

        int id_disertante = 0;

        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "INSERT INTO disertantes (" +
                "nombre, " + // 1
                "ciudad, " + // 2
                "id_grado_academico, " + // 4
                "correo, " + // 5
                "telefono, " + // 6
                "extension, " + // 7
                "sistesis_curricular, " + // 8
                "institucion_procedencia, " + // 9
                "id_cargo_ocupacion) " + // 15
                "VALUES ('" +
                d.get(1) + "','" +
                d.get(2) + "','" +
                d.get(4) + "','" +
                d.get(5) + "','" +
                d.get(6) + "','" +
                d.get(7) + "','" +
                d.get(8) + "','" +
                d.get(9) + "','" +
                d.get(15) + "');";

        try {
            stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                id_disertante = rs.getInt("id");
            }
            stmt.close();
            c.commit();
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.out.println(sql);
        }

        return id_disertante;
    }

    public void insertDiserMater(Map d) {
        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "INSERT INTO diser_mater (" +
                "id_disertante, " +
                "id_materia) " + // 10-14
                "VALUES ('" +
                d.get(0) + "','" +
                d.get(1) + "');";

        try {
            stmt.execute(sql);
            stmt.close();
            c.commit();
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertEvalFecha(Map d) {
        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "INSERT INTO eval_fecha (" +
                "id_evaluacion, " +
                "fecha_evento) VALUES ('" + // 24
                d.get(0) + "','" +
                d.get(1) + "');";

        try {
            stmt.execute(sql);
            stmt.close();
            c.commit();
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.out.println(sql);
        }
    }

    public int insertEvaluacion(Map d) {
        int id_evaluacion = 0;
        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "INSERT INTO evaluacion (" ;
            if(d.get(70) != null) { // VALIDA QUE TENGA CLASI_EVENTO Y NIVEL EVENTO
                sql += "clasi_evento, " +
                "nivel_evento, ";
            }
            sql += "id_disertante, " + // 1
                "id_casa_cultura, " + // 3
                "ev_asis, " + // 16
                "prom_asis, " + // 17
                "evento, " ; // 18
            if ( (d.get(19) != null) || (d.get(21) != null) ){// VALIDA QUE TENGA MODULO Y SESSION
                sql += "modulo, " + // 19 o 21
                "sesion, " ; // 20 o 22
            }
            sql += "tipo_evento, " + // 23
                "prom_dir, " + // 25
                "p1, " + // 26
                "p2, " + // 27
                "p3, " + // 28
                "p4, " + // 29
                "p5, " + // 30
                "p6, " + // 31
                "p7, " + // 32
                "p8, " + // 33
                "p9, " + // 34
                "p10) VALUES ('"; // 35
            if(d.get(70) != null) {  // VALIDA QUE TENGA CLASI_EVENTO Y NIVEL EVENTO
                sql += d.get(71) + "','" +
                        d.get(70) + "','";
            }
            sql += d.get(1) + "','" +
                d.get(3) + "','" +
                d.get(16) + "','" +
                d.get(17) + "','" +
                d.get(18) + "','" ;
                if(d.get(19) != null) {
                    sql += d.get(19) + "','" ;
                }
                if(d.get(20) != null) {
                    sql += d.get(20) + "','" ;
                }
                if(d.get(21) != null) {
                    sql += d.get(21) + "','" ;
                }
                if(d.get(22) != null) {
                    sql += d.get(22) + "','" ;
                }
                if(d.get(23) != null) {     // TIPO EVENTO
                    sql += d.get(23) + "','" ;
                } else {
                    sql += "0','" ;
                }
                if(d.get(25).toString().length() > 3) { // VALIDA PROMEDIO
                    sql += d.get(25).toString().substring(0,3) + "','";
                } else {
                    sql += d.get(25).toString() + "','";
                }
                sql += getValueCalif(d.get(26).toString()) + "','" +
                getValueCalif(d.get(27).toString()) + "','" +
                getValueCalif(d.get(28).toString()) + "','" +
                getValueCalif(d.get(29).toString()) + "','" +
                getValueCalif(d.get(30).toString()) + "','" +
                getValueCalif(d.get(31).toString()) + "','" +
                getValueCalif(d.get(32).toString()) + "','" +
                getValueCalif(d.get(33).toString()) + "','" +
                getValueCalif(d.get(34).toString()) + "','" +
                getValueCalif(d.get(35).toString()) + "');";

        try {
            stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                id_evaluacion = rs.getInt("id");
            }
            stmt.close();
            c.commit();
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.out.println(sql);
        }
        return id_evaluacion;
    }

    public String getValueCalif(String val) {
        switch (val) {
            case "Excelente" :
                return "5";
            case "Bueno" :
                return "4";
            case "Regular" :
                return "3";
            case "Deficiente" :
                return "2";
            case "Malo" :
                return "1";
            default:
                return "0";
        }
    }

    // Obener id_catalogo
    public int getIdDisertante(String nombre) {
        int idDisertante = 0;

        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "SELECT id FROM disertantes WHERE nombre = '"+ nombre +"'";
        try {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                idDisertante = rs.getInt("id");
            }
            stmt.close();
            c.commit();
            c.close();
        } catch(SQLException e) {
            System.out.println(e);
            System.out.println(sql);
        }
        return idDisertante;
    }

    // Obener id_catalogo
    public int getCatalogo(String catalogo, String clave_catalogo) {
        int id_catalogo = 0;

        if(catalogo.toString().trim().length() < 1) {
            return id_catalogo;
        }

        try {
            c = con.getConnection();
            c.setAutoCommit(false);
            stmt = c.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String sql = "SELECT id FROM catalogos WHERE catalogo like '"+ catalogo +"%'" +
                " AND cve_catalogo = '" + clave_catalogo + "' limit 1";

        try {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                id_catalogo = rs.getInt("id");
            }

            stmt.close();
            c.commit();
            c.close();
        } catch(SQLException e) {
            System.out.println(e);
            System.out.println(sql);
        }
        return id_catalogo;
    }

}
