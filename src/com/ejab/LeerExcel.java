package com.ejab;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.text.Keymap;

public class LeerExcel {

    public static void main(String args[]) throws IOException{
        FileInputStream file = new FileInputStream(new File("/Users/edsonamaya/Downloads/RUD.xlsx"));
        // Crear el objeto que tendra el libro de Excel
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        MapearPostgres mp = new MapearPostgres();
	/*
	 * Obtenemos la primera pestaña a la que se quiera procesar indicando el indice.
	 * Una vez obtenida la hoja excel con las filas que se quieren leer obtenemos el iterator
	 * que nos permite recorrer cada una de las filas que contiene.
	 */
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        int[] disertantes = {1,2,4,5,6,7,8,9,15};
        int[] diser_mater = {10,11,12,13,14};
        int[] eval_fecha = {24}; // NUMERIC 24
        int[] evaluacion = {3,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35}; // NUMERIC 17, 25

        Row row;
        int index = 0;
        // Recorremos todas las filas para mostrar el contenido de cada celda
        while (rowIterator.hasNext()){
            Map disertante = new HashMap();
            Map diserMater = new HashMap();
            Map evalFecha = new HashMap();
            Map eval = new HashMap();

            int idEvento = 0;
            int idDisertante = 0;

            row = rowIterator.next();

            // DISERTANTES
            ciclo_diser:
            for(int d = 0; d < disertantes.length; d++) {
                try {
                    Cell c = row.getCell(disertantes[d]);
                    if (c != null) {
                        c.setCellType(CellType.STRING);
                        if (c.getStringCellValue() != null || c.getStringCellValue() != "") {
                            //Obtiene id de catalogo grado_academico
                            if (disertantes[d] == 1) {
                                idDisertante = mp.getIdDisertante(c.getStringCellValue());
                                System.out.println("VALOR ID DISERTANTE =" + idDisertante);
                                if (idDisertante > 0) {
                                    break ciclo_diser;
                                } else {
                                    idDisertante = 0;
                                    disertante.put(disertantes[d], c.getStringCellValue());
                                }
                            } else if (disertantes[d] == 4) {
                                String ga = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "grado_academico"));
                                if (ga != null) {
                                    disertante.put(disertantes[d], ga);
                                } else {
                                    disertante.put(disertantes[d], "1");
                                }
                            } else if (disertantes[d] == 15) { //Obtiene id de catalogo cargo_ocupacion
                                disertante.put(disertantes[d], String.valueOf(mp.getCatalogo(c.getStringCellValue(), "cargo_ocupacion")));
                            } else {
                                disertante.put(disertantes[d], c.getStringCellValue());
                            }
                        } else if (c.getStringCellValue() == "" || c.getStringCellValue() == null) {
                            disertante.put(disertantes[d], " ");
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            if(idDisertante == 0) {
                idDisertante = mp.insertDisertantes(disertante);
                // DISER MARTER
                for(int dm = 0; dm < diser_mater.length; dm++) {
                    diserMater = new HashMap();
                    diserMater.put(0,String.valueOf(idDisertante));
                    Cell c = row.getCell(diser_mater[dm]);
                    if (null != c) {
                        c.setCellType(CellType.STRING);
                        if (c.getStringCellValue() != null || c.getStringCellValue() != "") {
                            //Obtiene id de catalogo materia
                            int val = mp.getCatalogo(c.getStringCellValue(), "materias");
                            if(val > 0) {
                                diserMater.put(1,val);
                                mp.insertDiserMater(diserMater);
                                //System.out.println(diserMater);
                            }
                        }
                    }
                }
            }


            eval.put(1,String.valueOf(idDisertante));
            // EVALUACION
            for(int e = 0; e < evaluacion.length; e++) {
                Cell c = row.getCell(evaluacion[e]);
                if(evaluacion[e] == 17) {
                    if (c != null) {
                        c.setCellType(CellType.NUMERIC);
                        if (c.getNumericCellValue() > 0) {
                            eval.put(evaluacion[e],String.valueOf(c.getNumericCellValue()));
                        } else {
                            eval.put(evaluacion[e],"");
                        }
                    }
                } else {
                    if (c != null) {
                        c.setCellType(CellType.STRING);
                        if (c.getStringCellValue() != null || c.getStringCellValue() != "") {
                            //Obtiene id de catalogo casa_cultura
                            if (evaluacion[e] == 3) {
                                eval.put(evaluacion[e], String.valueOf(mp.getCatalogo(c.getStringCellValue(), "casa_cultura")));
                            } else if (evaluacion[e] == 19) { //Obtiene id de catalogo modulo_djadh
                                String val = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "mod_djadh"));
                                if (!val.trim().equals("0")) {
                                    eval.put(evaluacion[e], val);
                                    eval.put(70, -1);
                                    eval.put(71, 1);
                                }
                            } else if (evaluacion[e] == 20) { //Obtiene id de catalogo sesion_djadh
                                String val = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "ses_djadh"));
                                if (!val.trim().equals("0")) {
                                    eval.put(evaluacion[e], val);
                                }
                            } else if (evaluacion[e] == 21) { //Obtiene id de catalogo modulo_dja
                                String val = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "mod_dja"));
                                if (!val.trim().equals("0")) {
                                    eval.put(evaluacion[e], val);
                                    eval.put(70, -2);
                                    eval.put(71, 1);
                                }
                                if(eval.get(19) == null && val.trim().equals("0")) {
                                    eval.put(evaluacion[e], "0");
                                }
                            } else if (evaluacion[e] == 22) { //Obtiene id de catalogo sesion_dja
                                String val = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "ses_dja"));
                                if (!val.trim().equals("0")) {
                                    eval.put(evaluacion[e], val);
                                }
                                if(eval.get(20) == null && val.trim().equals("0")) {
                                    eval.put(evaluacion[e], "0");
                                }
                            } else if (evaluacion[e] == 23) { //Obtiene id de catalogo Tipo evento
                                String val = String.valueOf(mp.getCatalogo(c.getStringCellValue(), "tipo"));
                                if (!val.trim().equals("0")) {
                                    eval.put(evaluacion[e], val);
                                }
                            } else {
                                eval.put(evaluacion[e], c.getStringCellValue());
                            }

                        } else if (c.getStringCellValue() == "" || c.getStringCellValue() == null) {
                            eval.put(evaluacion[e], " ");
                        }
                    }
                }
            }

            idEvento = mp.insertEvaluacion(eval);
            //System.out.println("VALOR DE ID EVENTO = " + idEvento);
            evalFecha.put(0,idEvento);
            // EVAL FECHA
            Cell c = row.getCell(eval_fecha[0]);
            if (null != c) {
                c.setCellType(CellType.NUMERIC);
                if (c.getDateCellValue() != null) {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    evalFecha.put(1, df.format(c.getDateCellValue()));
                    mp.insertEvalFecha(evalFecha);
                }
            }
            idDisertante = 0;
            idEvento = 0;
            /*System.out.println(disertante);
            System.out.println(evalFecha);
            System.out.println(eval);*/

        }

        // cerramos el libro excel
        workbook.close();
    }

}

